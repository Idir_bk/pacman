#include "fontom.h"
#include "primitives.h"
#include "pile.h"
#include <string.h>
static void deplacementpcc(fontom *fon ,laby lab,pacman pac,int *dx,int *dy);
static void deplacementalea(fontom *fon ,laby lab,pacman pac,int *dx,int *dy);
static void subpcc(int **l, int v, int x0, int y0, int x1, int y1, int w) {
  int i;
  const int d[][2] = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
  l[y0][x0]  = v++;
  if(x0 == x1 && y0 == y1) return;
  for(i = 0; i < 4; ++i) {
    int nx = x0 + d[i][0];
    int ny = y0 + d[i][1];
    
    if( (ny >0 && ny < 19 && nx >0 && nx < 27) && (l[ny][nx]  !=-1) && (l[ny][nx]  == 0 || l[ny][nx]  > v))
    {    
     
      subpcc(l, v, nx, ny, x1, y1, w);
    }
  }
}

static void pcc(laby lab,int x0, int y0, int x1, int y1) {
  int **copy = malloc(27  * sizeof (int *));
   
  int **l = malloc(19*sizeof(int*));
  for(int i =0;i<19;i++)
  {
    copy[i]= malloc(27*sizeof(int));
  }
      for(int i = 0;i<19;i++)
  {
    for(int j=0 ; j<27;j++)
    { if (lab.l[j][i]   ==  -1)
      {
        copy[i][j]= -1;

      }else{
        copy[i][j] =0;
      }
    }
   
  }
  copy[9][0] =-1;
  copy[9][26] =-1;
   
  

 
  
  subpcc(copy, 1, x0, y0, x1, y1,27);
  while(x1 != x0 || y1 != y0) {
    int i;
    const int d[][2] = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
    push(y1);
    push(x1);
    for(i = 0; i < 4; ++i) {
      int nx = x1 + d[i][0];
      int ny = y1 + d[i][1];
      if(copy[ny][nx] == copy[y1][x1] - 1) {
	x1 = nx;
	y1 = ny;
	break;
      }
    }
  }}

drawFontom(fontom fon){
    int x = fon.px*30;
    int y = fon.py*30;
    vec4 v={1.0f, 0.6f, 0.8f, 1.0f};
    if (fon.id == 1)
    {
v.x =1.0f;
v.y = 0.6f;
v.z = 0.8f;
    }
    if (fon.id == 2)
    {
      v.x = 1.0f;
      v.y = 0.0f;
      v.z = 0.0f;
    }
    if (fon.id == 3)
    {
      v.x = 1.0f;
      v.y = 1.0f;
      v.z = 1.0f;
    }

    triangle_t a = {{ 
   
			 
			  
			  { x+5,y+5, v, { 10.0f, 0.0f} },
         {  x + 25, y+5, v, { 0.0f, 10.0f} }, 
        { x+15, y+25, v, { 0.0f, 10.0f} } 
    }};
     fillTriangle(&a);

}
void moveFontom(fontom *fon ,laby lab,pacman pac,int *dx,int *dy,int colision)
{ if(fon->id == 1){
  deplacementpcc(fon,lab,pac,dx,dy);
}
if(fon->id == 2)
{ 
deplacementalea(fon,lab,pac,dx,dy);
}
if(fon->id == 3)
{
  int c = rand()%3;
  if(!c)
  {
    deplacementpcc(fon,lab,pac,dx,dy);
  }else 
  {
    deplacementalea(fon,lab,pac,dx,dy);
  }
}

}

//cette fonction deplacement permet au fantôme de coller au pacman
static void deplacementpcc(fontom *fon ,laby lab,pacman pac,int *dx,int *dy)
{
if(pac.cx != 0 && pac.cx != 26 )
{
    pcc(lab,fon->px, fon->py, pac.cx,pac.cy);
    if(!empty()){
    fon->px = pop();
    fon->py = pop();
    }
    while(!empty())
    {
      pop();
    }
}
}
static void deplacementalea(fontom *fon ,laby lab,pacman pac,int *dx,int *dy)
{
int d[][2] ={{1,0},{0,-1},{-1,0},{0,1}};
  if(fon->px +*dx <0 )
  {
    *dx = 1;
  }
  else{
  if(fon->px +*dx >26 )
  {
    *dx = -1;
  }
  }
  if(lab.l [fon->px+*dx][fon->py+*dy] == -1 || (*dx+*dy)==0)
  {
    do{
      int c = rand()%4;
      *dx =d[c][0];
      *dy =d[c][1];
    }while(lab.l [fon->px+*dx][fon->py+*dy] == -1);

  }
  fon->px = fon->px+*dx;
  fon->py = fon->py+*dy;
}
