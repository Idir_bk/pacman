#include <GL4D/gl4dp.h>

#include "pacman.h"
typedef struct fontome fontom;

struct fontome{
    int px,py;
    int dx,dy;
    int id;
};
extern void drawFontom(fontom fon);
extern void moveFontom(fontom *fon ,laby lab,pacman pac,int *dx,int *dy,int colision);