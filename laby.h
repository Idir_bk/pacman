#include <GL4D/gl4dp.h>
typedef struct laby laby;

struct laby{
    int ** l;
    int maxi;
    int maxj;
};

extern void drawlaby(laby lab);
extern int ** initLaby();