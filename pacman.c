
#include "pacman.h"
#include "primitives.h"


void drawPacman(pacman pac)
{   float angle;
    GLubyte r = 255, g = 255, b = 0;
    int ray = 15;
    float valueD,valueF;

    if (pac.orientation == 'D')
    {
      valueD = 0.5f;
      valueF =  1.8f;
    
  }
  if(pac.orientation == 'G')
  {
      valueD = -2.5f;
      valueF =  0.8f;
  }
  if(pac.orientation == 'N')
  {
      valueD = 2.2f;
      valueF = 2.3f;
  }
  if(pac.orientation == 'S')
  {
      valueD = -1.0f;
      valueF = 1.3f;
  }
  if(pac.etatBouche == 0)
  {
    valueD = 0.0f;
      valueF = 2.0f;
  }
  for(angle = valueD; angle < valueF * M_PI; angle += 0.005f) {
    drawLine(pac.cx*30 + 15, pac.cy*30 + 15, pac.cx*30 + 15 + ray * cos(angle), pac.cy*30 + 15 + ray * sin(angle), RGB(r, g, b));
}}
void movePacman(pacman *pac ,laby lab,int dx,int dy,int * score)
{ 
  
  if(pac->cx+dx == lab.maxi)
{ if(lab.l[pac->cx][pac->cy] != 0 && lab.l[pac->cx][pac->cy] != -2)
  {(*score)++;}
  lab.l[pac->cx][pac->cy]&=0;
  pac->cx = 0;
  pac->orientation = 'D';
  pac->etatBouche = (pac->etatBouche + 1)% 2;
  
  return;
}
if(pac->cx+dx < 0)
{if(lab.l[pac->cx][pac->cy] != 0 && lab.l[pac->cx][pac->cy] != -2 )
  {(*score)++;}
  lab.l[pac->cx][pac->cy]&=0;
  pac->cx = lab.maxi-1;
  pac->orientation = 'G';
  pac->etatBouche = (pac->etatBouche + 1)% 2;
  
  return;
}
if(dx == 0 && dy == 0)
    {return;
    } 

  if(lab.l[pac->cx+dx][pac->cy+dy] != -1)
  {if(lab.l[pac->cx][pac->cy] != 0 && lab.l[pac->cx][pac->cy] != -2)
    {(*score)++;}
    lab.l[pac->cx][pac->cy]&=0;
    pac->cx+= dx;
    pac->cy+= dy;
    pac->etatBouche = (pac->etatBouche + 1)% 2;
    if(dx == 1)
    {
      pac->orientation = 'D';
    }else{
      if(dx == -1)
      {
        pac->orientation = 'G';
      }else{
        if(dy == 1)
        {
          pac->orientation = 'N';
        }else{
          if(dy == -1)
          {
            pac->orientation = 'S';
          }
        }
      }
    }
  }
}

