#include <GL4D/gl4dp.h>
#include "laby.h"
typedef struct pacman pacman;

struct pacman{
    int cx,cy;
    char orientation;
    int etatBouche;
};

extern void drawPacman(pacman pac);
extern void movePacman(pacman *pac ,laby lab,int dx,int dy,int * score);