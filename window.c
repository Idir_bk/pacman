/*!\file window.c
 * \brief GL4Dummies, exemple simple 2D avec GL4Dummies
 * \author Farès BELHADJ, amsi@ai.univ-paris8.fr
 * \date February 04 2018, modified on March 06, 2020 by 
 * adding Visual Studio configuration files.
 */

/* inclusion des entêtes de fonctions de gestion de primitives simples
 * de dessin. La lettre p signifie aussi bien primitive que
 * pédagogique. */
#include <GL4D/gl4dp.h>
#include "primitives.h"

#include <time.h>
#include "fontom.h"

int delta[][2] = {{0,0},{0,0},{0,0},{0,0}};
int dx =0;
int dy = 0;
int a =0;
int v =5;
int debut = 0;
static void keyup(int keycode);


pacman p = { 1, 1,'D',1};
fontom f = { 11, 9,0,0,1};
fontom f2 = { 12, 9,0,0,2};
fontom f3 = { 13 ,9,0,0,3};
laby lab ;
int score = 0;


static void reinitialisation()
{v--;
if(!v)
{
    v =5;
    lab.l =initLaby();
    score = 0;
}
dx =0;
dy = 0;
a =0;

p.cx = 1;
p.cy = 1;
p.orientation = 'D';
f.px =12; 
f.py=  9;
f2.px = 11;
f2.py = 9; 
f3.px = 13;
f3.py = 9;
debut = 0;
}

Uint32 move(Uint32 intervalle){
 if (debut)
 {
 movePacman(&p ,lab,dx,dy,&score);
 if((p.cx == f.px && p.cy == f.py) || (p.cx == f2.px && p.cy == f2.py) || (p.cx == f3.px && p.cy == f3.py) )
 {reinitialisation();
  
  return intervalle;
 }

 if ( a  == 0)
   {moveFontom(&f ,lab , p,&delta[1][0],&delta[1][1],6);
    moveFontom(&f2 ,lab , p,&delta[0][0],&delta[0][1],6);
    moveFontom(&f3 ,lab , p,&delta[2][0],&delta[2][1],6);
    a =1;
    if((p.cx == f.px && p.cy == f.py) || (p.cx == f2.px && p.cy == f2.py) || (p.cx == f3.px && p.cy == f3.py))
 { reinitialisation();
   return intervalle;
 }
   }else{
     a =0;
   }
 
 if(score == 240)
    {
      fprintf(stderr, "winn");
      score = 0;
      lab.l =initLaby();
    lab.maxi = 27;
    lab.maxj = 19;
    }
    }
  return intervalle;
}




/* inclusion des entêtes de fonctions de création et de gestion de
 * fenêtres système ouvrant un contexte favorable à GL4dummies. Cette
 * partie est dépendante de la bibliothèque SDL2 */
#include <GL4D/gl4duw_SDL2.h>

static void draw(void);

/*!\brief créé la fenêtre, un screen 2D effacé en noir et lance une
 *  boucle infinie.*/
int main(int argc, char ** argv) {
  lab.l =initLaby();
  lab.maxi = 27;
  lab.maxj = 19;
  
  
  /* tentative de création d'une fenêtre pour GL4Dummies */
  if(!gl4duwCreateWindow(argc, argv, /* args du programme */
			 "Pacman", /* titre */
			 10, 10, 810,600, /* x,y, largeur, heuteur */
			 GL4DW_SHOWN) /* état visible */) {
    /* ici si échec de la création souvent lié à un problème d'absence
     * de contexte graphique ou d'impossibilité d'ouverture d'un
     * contexte OpenGL (au moins 3.2) */
     
    return 1;
  }
  

  /* création d'un screen GL4Dummies (texture dans laquelle nous
   * pouvons dessiner) aux dimensions de la fenêtre */
  gl4dpInitScreen();
  //gl4dpInitScreenWithDimensions(160, 120);


  /* décommentez la ligne suivante pour effacer l'écran en bleu foncé. */
  //gl4dpClearScreenWith(RGB(0, 0, 155));

  gl4duwDisplayFunc(draw);
  gl4duwKeyUpFunc(keyup);
 
  /* boucle infinie pour éviter que le programme ne s'arrête et ferme
   * la fenêtre immédiatement */
   SDL_AddTimer(100,move,NULL);
  gl4duwMainLoop();
 
  return 0;
}
static void keyup(int keycode) {
  
  debut = 1;
  switch(keycode) {
  case GL4DK_ESCAPE:
  free(lab.l);
    exit(0);
    break;
  case GL4DK_LEFT:
  if(p.cx+1 != lab.maxi && p.cx !=0  && lab.l[p.cx-1][p.cy] != -1)
  {
    dx = -1;
    dy = 0;
  }
   
     
    break;
  case GL4DK_RIGHT:
  if(p.cx+1 != lab.maxi && p.cx !=0 &&lab.l[p.cx+1][p.cy] != -1)
    {
        dx =1;
      dy =0;
      }

   
    break;
  case GL4DK_DOWN:
  if(p.cx+1 != lab.maxi && p.cx !=0  &&lab.l[p.cx][p.cy-1] != -1)
   { dx =0;
    dy = -1;}
   
     
    break;
  case GL4DK_UP: 
  if(p.cx+1 != lab.maxi && p.cx !=0 &&lab.l[p.cx][p.cy+1] != -1)
   { dx = 0;
    dy = 1;}
  
    break;
  default:
    fprintf(stderr, "La touche <%d> est pressee\n", keycode);
    break;}
    
  
   
  
    
 
   

}
void draw(void) {
  
  
  /* effacement du screen en cours en utilisant la couleur par défaut,
   * le noir */
  gl4dpClearScreen();
  
  drawlaby(lab );
  drawPacman(p);
  drawFontom(f);
  drawFontom(f2);
  drawFontom(f3);
  int y = 19*30;
  for (int i =0 ;i<v;i++)
  
  {int x =i * 30;
    triangle_t a = {{ 
   
			 
			  
			  { x+5,y+25, {1.0f, 0.0f, 0.0f, 1.0f}, { 10.0f, 0.0f} },
         {  x + 25, y+25, {1.0f, 0.0f, 0.0f, 1.0f}, { 0.0f, 10.0f} }, 
        { x+15, y+5, {1.0f, 0.0f, 0.0f, 1.0f}, { 0.0f, 10.0f} } 
    }};
     fillTriangle(&a);
  }
  //drawbloc(10,10);
  /* for(i = 0; i < 3; ++i) */
  /*   drawLine(t.p[i].x, t.p[i].y, t.p[(i + 1) % 3].x, t.p[(i + 1) % 3].y, -1); */

  
  gl4dpScreenHasChanged();
  /* fonction permettant de raffraîchir l'ensemble de la fenêtre*/
  gl4dpUpdateScreen(NULL);
  /*MAJ on bouge tout le monde pour voir */
  
  /*MAJ delai si nécessaire*/
  /*SDL_Delay(50);*/
}
